package ru.pixelsky.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import ru.pixelsky.chat.channel.ChatChannel;
import ru.pixelsky.chat.channel.EmptyChatChannel;
import ru.pixelsky.chat.channel.SimpleChatChannel;

import java.io.File;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class XChat extends JavaPlugin {
    public static final String PERMISSION_COLOR_MESSAGE = "xchat.chat.color";

    private static XChat instance;

    private Logger logger;
    private Map<String, ChatChannel> chatChannels;
    private Map<Player, ChatChannel> playerChannels;
    private CommandListener commandListener;
    private ChatChannel defaultChatChannel;
    private ChatChannel emptyChannel = new EmptyChatChannel();

    public static FileConfiguration config;

    @Override
    public void onEnable() {
        instance = this;
        logger = getLogger();

        /* Events and commands*/
        commandListener = new CommandListener(this);
        getServer().getPluginManager().registerEvents(new ChatListener(this), this);

        /* Channels */
        chatChannels = new HashMap<>();
        playerChannels = new HashMap<>();
        loadConfig();
        registerChannel(emptyChannel);
    }

    public void setChatChannel(Player player, ChatChannel channel) {
        if (channel instanceof SimpleChatChannel) {
            SimpleChatChannel simpleChatChannel = (SimpleChatChannel) channel;
            if (!player.hasPermission(simpleChatChannel.getWritePermission())) {
                player.sendRawMessage(ChatColor.RED + "У вас нет прав на " + ((SimpleChatChannel) channel).getLocalizedName());
                return;
            }
        }

        playerChannels.put(player, channel);
        player.sendRawMessage(ChatColor.GREEN + "Вы перешли на " + channel.getLocalizedName());
    }

    public void setDefaultChatChannel(Player player) {
        playerChannels.put(player, emptyChannel);
        setChatChannel(player, defaultChatChannel);
    }

    public ChatChannel getChatChannel(Player player) {
        return playerChannels.get(player);
    }

    public ChatChannel getChatChannel(String command) {
        return chatChannels.get(command);
    }

    public static XChat getInstance() {
        return instance;
    }

    private void loadConfig() {
        saveDefaultConfig();
        config = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "config.yml"));

        ConfigurationSection channelsSection = config.getConfigurationSection("channels");
        for (String channelCodename : channelsSection.getKeys(false)) {
            registerChannel(channelCodename, channelsSection.getConfigurationSection(channelCodename));
        }
    }

    private void registerChannel(String codename, ConfigurationSection section) {
        int range = section.getInt("range", 0);
        String name = section.getString("name");
        String format = section.getString("format");
        List<String> commands = section.getStringList("commands");
        int priceId = section.getInt("price", 0);

        SimpleChatChannel chatChannel = new SimpleChatChannel();
        chatChannel.setRange(range);
        chatChannel.setName(name);
        chatChannel.setFormat(ChatUtils.translateColorCodes(format));
        chatChannel.setReadPermission("xchat.read." + codename);
        chatChannel.setWritePermission("xchat.write." + codename);
        chatChannel.setCommands(commands);

        if (priceId > 0)
            chatChannel.setPrice(new ItemStack(priceId, 1));

        registerChannel(chatChannel);

        logger.info(codename + " channel registered");
    }

    private void registerChannel(ChatChannel channel) {
        if (defaultChatChannel == null)
            defaultChatChannel = channel;

        for (String command : channel.getCommands()) {
            PluginCommand cmd = getCommand(command);
            if (cmd != null) {
                cmd.setExecutor(commandListener);
                chatChannels.put(command, channel);
            } else {
                logger.warning(command + " is not in plugin.yml");
            }
        }
    }
}
