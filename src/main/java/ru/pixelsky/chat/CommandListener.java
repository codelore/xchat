/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.chat;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.pixelsky.chat.channel.ChatChannel;

public class CommandListener implements CommandExecutor {
    private XChat plugin;

    public CommandListener(XChat plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        ChatChannel channel = plugin.getChatChannel(cmd.getName());

        if (channel != null) {
            plugin.setChatChannel((Player) sender, channel);
        } else {
            ((Player) sender).sendRawMessage(ChatColor.RED + "Команда " + cmd.getName() + " не привязана ни к какому каналу");
        }

        return true;
    }

}
