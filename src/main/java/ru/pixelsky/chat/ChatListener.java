package ru.pixelsky.chat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import ru.pixelsky.chat.channel.ChatChannel;

public class ChatListener implements Listener {
    private XChat plugin;

    public ChatListener(XChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage("");
        Player player = e.getPlayer();
        plugin.setDefaultChatChannel(player);
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        e.setQuitMessage("");
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent e) {
        Player sender =  e.getPlayer();
        ChatChannel channel = plugin.getChatChannel(sender);

        e.getRecipients().clear();

        if(!channel.sendMessage(sender, e.getMessage()))
            e.setCancelled(true);
    }
}
