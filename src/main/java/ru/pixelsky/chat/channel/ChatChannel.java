package ru.pixelsky.chat.channel;

import org.bukkit.entity.Player;

import java.util.List;

public interface ChatChannel {
    /**
     * Отправляет сообщение, при этом с игрока могут списаться средства
     * @return было ли отправлено сообщение
     */
    boolean sendMessage(Player sender, String message);

    /** Отправляет сообщение, двже если у игрока нет средств на его отправку */
    void sendMessageFree(Player sender, String message);

    /** Отображается в сообщении "Вы переключились на YYY" **/
    String getLocalizedName();

    /** Команды, с помощью которых можно переключиться на этот канал **/
    List<String> getCommands();
}
