package ru.pixelsky.chat.channel;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;

public class EmptyChatChannel implements ChatChannel {
    @Override
    public boolean sendMessage(Player sender, String message) {
        sender.sendMessage(ChatColor.RED + "Не выбран канал");
        return false;
    }

    @Override
    public void sendMessageFree(Player sender, String message) {
        sender.sendMessage(ChatColor.RED + "Не выбран канал");
    }

    @Override
    public String getLocalizedName() {
        return "<отсутствие канала>";
    }

    @Override
    public List<String> getCommands() {
        return Collections.emptyList();
    }
}
