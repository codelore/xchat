package ru.pixelsky.chat.channel;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import ru.pixelsky.chat.ChatUtils;
import ru.pixelsky.chat.XChat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SimpleChatChannel implements ChatChannel {
    /** Цена, которую надо оплатить для использования канала */
    private ItemStack price;

    /** Радиус, на котором действует канал, если канал не ограничен по радиусу - 0 */
    private int range;

    /** Какая персисия нужна для прослушки канала */
    private String readPermission;

    /** Какая пермиссия нужна для написания в канал */
    private String writePermission;

    /** Команды */
    private List<String> commands = Collections.emptyList();

    /** Название */
    private String name;

    /**
     * Формат чата, основные параметры: %prefix %player %suffix %message %count
     * Можно использовать & для задания цветов
     */
    private String format = "&6%prefix%player%suffix: &f%message";


    /** Пытаеться забрать price, затем вызывает sendMessageFree **/
    @Override
    public boolean sendMessage(Player sender, String message) {
        if (!hasWritePerm(sender))
            return false;
        if (!payPrice(sender)) {
            Material material = price.getType();
            String materialName = material != null ? material.name() : "X" + price.getTypeId();
            sender.sendMessage(ChatColor.RED + "У Вас нет товара для оплаты сообщения ("  + materialName + ")");
            return false;
        }

        sendMessageFree(sender, message);
        return true;
    }

    /**
     * Пытается отнять у игрока price
     * @return канал бесплатный или предмет удалось отнять
     */
    private boolean payPrice(Player player) {
        if (price == null)
            return true;

        Inventory inv = player.getInventory();
        return inv.removeItem(price).isEmpty();
    }

    /** Может ли этот игрок читать этот чат */
    private boolean hasReadPerm(Player player) {
        return readPermission == null || player.isOp() || player.hasPermission(readPermission);
    }

    /** Может ли этот игрок читать этот чат */
    private boolean hasWritePerm(Player player) {
        return writePermission == null || player.isOp() || player.hasPermission(writePermission);
    }

    /** Выполняет форматирование сообщение и его отправку **/
    @Override
    public void sendMessageFree(Player sender, String message) {
        if (sender.isOp() || sender.hasPermission(XChat.PERMISSION_COLOR_MESSAGE)) {
            message = ChatUtils.translateColorCodes(message);
        }

        List<Player> recipients = getRecipients(sender);

        String formattedMessage = format;
        formattedMessage = formattedMessage.replace("%player", sender.getDisplayName());
        formattedMessage = formattedMessage.replace("%prefix", ChatUtils.getPrefix(sender));
        formattedMessage = formattedMessage.replace("%suffix", ChatUtils.getSuffix(sender));
        formattedMessage = formattedMessage.replace("%count", Integer.toString(recipients.size() - 1)); // -1 himself.
        formattedMessage = formattedMessage.replace("%message", message);

        XChat.getInstance().getLogger().info(formattedMessage);

        for (Player player : recipients) {
            player.sendRawMessage(formattedMessage);
        }
    }

    private boolean isInRange(Player sender, Player listener) {
        return range <= 0 ||
                (sender.getWorld() == listener.getWorld() &&
                        sender.getLocation().distanceSquared(listener.getLocation()) < range * range);
    }

    /** Вычисляет получателей сообщения */
    private List<Player> getRecipients(Player sender) {
        List<Player> allPlayers = Arrays.asList(Bukkit.getOnlinePlayers());
        List<Player> recipients = allPlayers;

        if (readPermission != null || range > 0) {
            recipients = new ArrayList<>();
            for (Player player : allPlayers) {
                if (!hasReadPerm(player)) {
                    /* Нет прав */
                    continue;
                }
                if (!isInRange(sender, player)) {
                    /* Слишком далеко */
                    continue;
                }
                recipients.add(player);
            }
        }

        return recipients;
    }

    @Override
    public String getLocalizedName() {
        return name;
    }

    @Override
    public List<String> getCommands() {
        return commands;
    }

    /*
    * Setter methods
    */

    public void setPrice(ItemStack price) {
        this.price = price;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public void setReadPermission(String readPermission) {
        this.readPermission = readPermission;
    }

    public void setWritePermission(String writePermission) {
        this.writePermission = writePermission;
    }

    public void setCommands(List<String> commands) {
        this.commands = commands;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public ItemStack getPrice() {
        return price;
    }

    public int getRange() {
        return range;
    }

    public String getReadPermission() {
        return readPermission;
    }

    public String getWritePermission() {
        return writePermission;
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        return format;
    }
}
