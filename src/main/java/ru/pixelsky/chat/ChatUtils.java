/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.pixelsky.chat;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

/**
 * @author Asura
 */
public class ChatUtils {
    protected static Pattern chatColorPattern = Pattern.compile("(?i)&([0-9A-F])");
    protected static Pattern chatMagicPattern = Pattern.compile("(?i)&([K])");
    protected static Pattern chatBoldPattern = Pattern.compile("(?i)&([L])");
    protected static Pattern chatStrikethroughPattern = Pattern.compile("(?i)&([M])");
    protected static Pattern chatUnderlinePattern = Pattern.compile("(?i)&([N])");
    protected static Pattern chatItalicPattern = Pattern.compile("(?i)&([O])");
    protected static Pattern chatResetPattern = Pattern.compile("(?i)&([R])");

    public static String translateColorCodes(String string) {
        if (string == null) {
            return "";
        }

        String newstring = string;
        newstring = chatColorPattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatMagicPattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatBoldPattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatStrikethroughPattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatUnderlinePattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatItalicPattern.matcher(newstring).replaceAll("\u00A7$1");
        newstring = chatResetPattern.matcher(newstring).replaceAll("\u00A7$1");
        return newstring;
    }

    public static String translateColorCodes(String string, PermissionUser user, String worldName) {
        if (string == null) {
            return "";
        }

        String newstring = string;
        if (user.has("xchat.chat.color", worldName)) {
            newstring = chatColorPattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        if (user.has("", worldName)) {
            newstring = chatMagicPattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        if (user.has("", worldName)) {
            newstring = chatBoldPattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        if (user.has("", worldName)) {
            newstring = chatStrikethroughPattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        if (user.has("", worldName)) {
            newstring = chatUnderlinePattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        if (user.has("", worldName)) {
            newstring = chatItalicPattern.matcher(newstring).replaceAll("\u00A7$1");
        }
        newstring = chatResetPattern.matcher(newstring).replaceAll("\u00A7$1");
        return newstring;
    }


    public static String getPrefix(Player player) {
        String prefix = PermissionsEx.getUser(player).getPrefix();
        return prefix != null ? translateColorCodes(prefix) : ChatColor.RESET.toString();
    }

    public static String getSuffix(Player player) {
        String suffix = PermissionsEx.getUser(player).getSuffix();
        return suffix != null ?  translateColorCodes(suffix) : ChatColor.RESET.toString();
    }
}
